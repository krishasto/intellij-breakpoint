# Read Me First
The following was discovered as part of building this project:

* The JVM level was changed from '1.8' to '17', review the [JDK Version Range](https://github.com/spring-projects/spring-framework/wiki/Spring-Framework-Versions#jdk-version-range) on the wiki for more details.

# Getting Started  
## Create Debug Configuration  
Copy [this file](idea/runConfigurations/demo [spring-boot_run].run.xml) to ./idea  

![Debug Config](images/intellij-debug-config.png)  

## Put breakpoint  
![Breakpoint](images/intellij-breakpoint.png)  

##  Start debugging 
within intellij for `demo [spring-boot_run]`  



