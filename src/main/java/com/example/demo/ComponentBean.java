package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ComponentBean {

    private String txtVar = "Just simple text";
    private Logger log = LoggerFactory.getLogger("ComponentBean");

    @PostConstruct
    private void init(){
        log.debug("Breakpoint txtVar = {}", txtVar);
    }

    public String txtInit(){
        return txtVar;
    }

}
