package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping( "/demo")
public class DemoController {

    Logger log = LoggerFactory.getLogger("DemoController");

    @Autowired
    private ComponentBean bean;

    private String fromInit = "";

    @PostConstruct
    public void init(){
        fromInit = bean.txtInit();
        log.debug("Break here. {}", fromInit);
    }

    @GetMapping("/get")
    public String getDemo(){
        return fromInit;
    }
}
